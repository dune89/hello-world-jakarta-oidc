# hello-world-jakarta-oidc



## Getting started

You can either start the App using Docker-Compose:
- go to demo/Dockerfile (adapt or remove coporate proxy in ENV variables)
- mvn build demo/pom.xml
- cd /demo
- docker compose up -d
- open localhost[:80]/home
- login with "demo" "demo"

Or you can setup an environment with
- Wildfly 27 Alpha 1 (use standalone-full.xml)
- Keycloak 19.0.1
- Import demo/demo-keycloack/DemoRealm.json (for Client and User)
- change auth url in demo/demo-web/WebContent/WEB-INF/oidc.json
- mvn build demo/pom.xml
- deploy demo/demo-application/target/demo-application...ear
- open localhost[:yourWildFlyPort]/home
- login with "demo" "demo"


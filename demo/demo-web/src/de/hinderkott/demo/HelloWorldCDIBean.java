package de.hinderkott.demo;

import java.io.Serializable;
import java.security.Principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.EJB;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;

@Named
@ViewScoped
public class HelloWorldCDIBean implements Serializable {

	private static final long serialVersionUID = -6503517043368074756L;

	public static final Log log = LogFactory.getLog(HelloWorldCDIBean.class);

	@EJB
	HelloWorldServiceLocal service;

	Principal ejbPrincipal;

	@PostConstruct
	public void init() {
		ejbPrincipal = service.getPrincipal();
	}

	public String getText() {
		return ejbPrincipal.getName();
	}

}

package de.hinderkott.demo;

import org.wildfly.security.http.oidc.RefreshableOidcSecurityContext;

public class User {
	RefreshableOidcSecurityContext oidcContext;

	public User(RefreshableOidcSecurityContext oidcContext) {
		this.oidcContext = oidcContext;
	}

	public String getFullName() {
		return oidcContext != null ? oidcContext.getIDToken().getName() : "unknown";
	}

	public String getEMail() {
		return oidcContext != null ? oidcContext.getIDToken().getEmail() : "unknown@unknown.de";
	}

	public RefreshableOidcSecurityContext getOidcContext() {
		return oidcContext;
	}

}

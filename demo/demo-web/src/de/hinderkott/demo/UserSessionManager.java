package de.hinderkott.demo;

import java.io.IOException;
import java.io.Serializable;
import java.security.Principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.wildfly.security.http.oidc.OidcPrincipal;
import org.wildfly.security.http.oidc.RefreshableOidcSecurityContext;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.context.ExternalContext;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Named;
import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@Named
@SessionScoped
public class UserSessionManager implements Serializable {

	public static final Log log = LogFactory.getLog(UserSessionManager.class);
	private static final String UIPATH_LOGGEDIN_INDEX = "/ui/index.xhtml";
	private static final long serialVersionUID = -7348231184394201753L;

	/**
	 * Kann ggf durch ebv user ausgetauscht werden..
	 */
	private User oidcUser;

	@PostConstruct
	private void init() {
		login();
	}

	public void login() {
		RefreshableOidcSecurityContext ctx = getSecurityContext();
		if (ctx != null) {
			oidcUser = new User(ctx);
		} else {
			log.info("no oidc ctx");
		}
	}

	private RefreshableOidcSecurityContext getSecurityContext() {
		Principal p = getExternalContext().getUserPrincipal();
		if (p != null && p instanceof OidcPrincipal) {
			OidcPrincipal<?> principal = (OidcPrincipal<?>) p;
			if (principal.getOidcSecurityContext() instanceof RefreshableOidcSecurityContext) {
				RefreshableOidcSecurityContext ctx = (RefreshableOidcSecurityContext) principal.getOidcSecurityContext();
				return ctx;
			}
		}
		return null;
	}

	/**
	 * Keep the session alive - to be called from client.
	 */
	public void keepAlive() {
	}

	public void loginRedirect() {
		oidcUser = null;
		getExternalContext().invalidateSession();
		try {
			getExternalContext().redirect(getExternalContext().getApplicationContextPath().concat(UIPATH_LOGGEDIN_INDEX));
		} catch (IOException e) {
			log.error("error redirecting for login auotmation");
		}
	}

	public void validateSession(boolean fromGuestSite) {
		if (oidcUser == null && getSecurityContext() != null) {
			log.info("validateSession.. loginRedirect");
			loginRedirect();
		}

		if (fromGuestSite && oidcUser != null) {
			log.warn("already auth user landed on guest site, redirect to internal area");
			try {
				getExternalContext().redirect(getExternalContext().getApplicationContextPath().concat(UIPATH_LOGGEDIN_INDEX));
			} catch (IOException e) {
				log.error("error redirecting for login auotmation");
			}
		}
	}

	public boolean isLoggedin() {
		return oidcUser != null;
	}

	public HttpServletRequest getRequest() {
		return (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
	}

	public HttpServletResponse getResponse() {
		return (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
	}

	public ExternalContext getExternalContext() {
		return FacesContext.getCurrentInstance().getExternalContext();
	}

	public ServletContext getServletContext() {
		return getExternalContext().getContext() instanceof ServletContext ? (ServletContext) getExternalContext().getContext() : null;
	}

	public User getOIDCUser() {
		return oidcUser;
	}
}

package de.hinderkott.demo;

import java.security.Principal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jakarta.annotation.Resource;
import jakarta.annotation.security.PermitAll;
import jakarta.ejb.EJBContext;
import jakarta.ejb.Stateless;

@Stateless
// @RolesAllowed({ "demo-role" })
@PermitAll
public class HelloWorldEJB implements HelloWorldServiceLocal {

	public static final Log log = LogFactory.getLog(HelloWorldEJB.class);

	@Resource
	protected EJBContext securityContext;

	@Override
	public Principal getPrincipal() {
		log.warn(securityContext.getCallerPrincipal());
		return securityContext.getCallerPrincipal();
	}

}

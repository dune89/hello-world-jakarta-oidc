package de.hinderkott.demo;

import java.security.Principal;

import jakarta.ejb.Local;

@Local
public interface HelloWorldServiceLocal {

	public Principal getPrincipal();

}
